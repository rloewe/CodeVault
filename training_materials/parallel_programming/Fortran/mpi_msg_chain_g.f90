! 
!  Copyright (C) 2015  CSC - IT Center for Science Ltd.
!
!  Licensed under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  Code is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  Copy of the GNU General Public License can be onbtained from
!  see <http://www.gnu.org/licenses/>.
! 

program basic

  use mpi
  use iso_fortran_env, only : REAL64

  implicit none
  integer, parameter :: size = 10000000
  integer :: rc, myid, ntasks, count
  integer :: status(MPI_STATUS_SIZE, 2)
  integer :: message(size)
  integer :: receiveBuffer(size)
  integer :: source, destination
  integer :: requests(2)

  real(REAL64) :: t0, t1

  call MPI_INIT(rc)
  call MPI_COMM_RANK(MPI_COMM_WORLD, myid, rc)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, ntasks, rc)

  message = myid

  if (myid < ntasks-1) then
     destination = myid + 1
  else
     destination = MPI_PROC_NULL
  end if

  ! Send and receive as defined in exercises
  if (myid > 0) then
     source = myid - 1
  else
     source = MPI_PROC_NULL
  end if

  call MPI_BARRIER(mpi_comm_world, rc)
  t0 = MPI_WTIME()

  ! Receive messages in the back ground
  call MPI_IRECV(Receivebuffer, size, MPI_INTEGER, source, &
       myid, MPI_COMM_WORLD, requests(1), rc)
  ! Send messages in the back ground
  call MPI_ISEND(message, size, MPI_INTEGER, destination, &
       myid + 1, MPI_COMM_WORLD, requests(2), rc)

  ! Blocking wait for messages
  call MPI_WAITALL(2, requests, status, rc)

  t1 = MPI_WTIME()

  write(*,'(A10,I3,A20,I8,A,I3,A,I3)') 'Sender: ', myid, &
       ' Sent elements: ', size, &
       '. Tag: ', myid + 1, '. Receiver: ', destination

  call MPI_GET_COUNT(status(:,1), MPI_INTEGER, count, rc)
  write(*,'(A10,I3,A20,I8,A,I3,A,I3)') 'Receiver: ', myid, &
       'received elements: ', count, &
       '. Tag: ', status(MPI_TAG, 1), &
       '. Sender:   ', status(MPI_SOURCE, 1)

  call print_ordered(t1-t0)

  call MPI_FINALIZE(rc)

contains

  subroutine print_ordered(t)
    implicit none

    real(REAL64), intent(in) :: t
    real(REAL64), dimension(:), allocatable :: times
    integer :: rank, ntasks, rc, i

    call MPI_COMM_RANK(MPI_COMM_WORLD, rank, rc)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, ntasks, rc)

    allocate(times(ntasks))

    call MPI_GATHER(t, 1, MPI_DOUBLE_PRECISION, times, 1, MPI_DOUBLE_PRECISION, &
         0, MPI_COMM_WORLD, rc)

    if (rank == 0) then
       write(*,*)
       write(*,*) 'Time elapsed in communication'
       do i=1, ntasks
          write(*, '(A4, I3, A, F6.3, A)') 'Rank', i-1, ':', times(i), 's'
       end do
    end if

    deallocate(times)

  end subroutine print_ordered

end program basic
