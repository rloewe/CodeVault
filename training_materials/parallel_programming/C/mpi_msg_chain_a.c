/*
   Copyright (C) 2015  CSC - IT Center for Science Ltd.

   Licensed under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Code is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   Copy of the GNU General Public License can be onbtained from
   see <http://www.gnu.org/licenses/>.
*/

#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>

void print_ordered(double t);

int main(int argc, char *argv[])
{
    int i, myid, ntasks;
    int size = 10000000;
    int *message;
    int *receiveBuffer;
    MPI_Status status;

    double t0, t1;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);

    /* Allocate message */
    message = malloc(sizeof(int) * size);
    receiveBuffer = malloc(sizeof(int) * size);
    /* Initialize message */
    for (i = 0; i < size; i++)
        message[i] = myid;

    MPI_Barrier(MPI_COMM_WORLD);
    t0 = MPI_Wtime();

    /* Send and receive messages as defined in exercise */
    if (myid < ntasks - 1) {
        MPI_Send(message, size, MPI_INT, myid + 1, myid + 1,
                 MPI_COMM_WORLD);
        printf("Sender: %d. Sent elements: %d. Tag: %d. Receiver: %d\n",
               myid, size, myid + 1, myid + 1);
    }

    if (myid > 0) {
        MPI_Recv(receiveBuffer, size, MPI_INT, myid - 1, myid,
                 MPI_COMM_WORLD, &status);
        printf("Receiver: %d. first element %d.\n",
               myid, receiveBuffer[0]);
    }

    t1 = MPI_Wtime();

    print_ordered(t1-t0);

    free(message);
    free(receiveBuffer);
    MPI_Finalize();
    return 0;
}

void print_ordered(double t)
{
  double *times;
  int i, rank, ntasks;

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &ntasks);

  times = (double *) malloc(ntasks * sizeof(double));

  MPI_Gather(&t, 1, MPI_DOUBLE, times, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  if (rank == 0) {
    printf("\n");
    printf("Time elapsed in communication\n");
    for (i=0; i < ntasks; i++)
      printf("Rank %d: %5.3f s\n", i, times[i]);
  }
}
