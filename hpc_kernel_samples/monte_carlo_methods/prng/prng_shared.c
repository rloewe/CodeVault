#include <stdlib.h> // rand_r, RAND_MAX, EXIT_FAILURE, atoi
#include <stdio.h> // printf
#include <time.h> // time

void read_arguments(int *seed, int *N, int argc, char *argv[]) {
    /*
     * parse command line arguments to obtain
     * - seed: seed for the pseudo random number generator
     * - N: number of random numbers to generate
     */

    // check if enough arguments are given
    if (argc != 3) { 
        // print usage information
        printf("USAGE %s <seed> <N>\n", argv[0]);
        printf("  seed (integer): seed for the pseudo random number generator\n");
        printf("                  a negative value indicates current timestamp\n");
        printf("  N (integer)   : number of random numbers to generate\n");
        // do not continue if number of arguments is invalid
        exit(EXIT_FAILURE);
    }

    // read seed for the pseudo random number generator
    // WARNING: according to the man page, atoi does not detect errors
    *seed = atoi(argv[1]);
    if (*seed < 0) {
        // use current timestamp as seed
        *seed = time(NULL);
    }

    // read number of random samples
    // WARNING: according to the man page, atoi does not detect errors
    *N = atoi(argv[2]);

}

