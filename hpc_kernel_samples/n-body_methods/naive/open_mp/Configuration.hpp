#pragma once

#include <cstddef>
#include <cstdlib>
#include <getopt.h>

struct Configuration {
	std::size_t NoIterations;
	std::size_t NoParticles;
};

Configuration parseArgs(int argc, char* argv[]) {
	int option;
	std::size_t noParticles{1000};
	std::size_t noIterations{1000};
	while ((option = getopt(argc, argv, "p:i:")) >= 0) {
		switch (option) {
		case 'p':
			noParticles = std::strtoul(optarg, nullptr, 10);
			break;
		case 'i':
			noIterations = std::strtoul(optarg, nullptr, 10);
			break;
		}
	}
	return {noIterations, noParticles};
}
