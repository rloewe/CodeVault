#ifndef BARNES_HUT_TREE_HPP
#define BARNES_HUT_TREE_HPP

#include "Tree.hpp"
#include "Box.hpp"

namespace nbody {
	using namespace std;

	class Node;

	class BarnesHutTree : public Tree {
	protected:
		static vector<Box> splitBB(Node* node);
		static bool splitNode(Node* current);
		virtual void update();
		virtual void init(vector<Body> bodies, Box domain);
		static void split(Node* current);
	public:
		BarnesHutTree(int parallelId);
		virtual ~BarnesHutTree();
		virtual void build(vector<Body> bodies);
		virtual void build(vector<Body> bodies, Box domain);
		virtual void mergeLET(vector<Body> bodies);
		virtual int numberOfChildren();
		static void splitSubtree(Node* root);
		virtual bool isCorrect();
	};
}

#endif
