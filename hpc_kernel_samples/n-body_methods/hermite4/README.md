=======
README
=======

# 1. Code sample name
Hermite4

# 2. Description of the code sample package
This example demonstrates a multi-core implementation of the Hermite 4th order nbody algorithm.

Additional pre-requisites:
* OpenMP

# 3. Release date
25 July 2015

# 4. Version history 
1.0

# 5. Contributor (s) / Maintainer(s) 
Evghenii Gaburov <evghenii.gaburov@surfsara.nl>

# 6. Copyright / License of the code sample
Apache 2.0

# 7. Language(s) 
C++ (C++11 is required)

# 8. Parallelisation Implementation(s)
multi-core CPU

# 9. Level of the code sample complexity 
Source-level implementation

# 10. Instructions on how to compile the code
Uses the CodeVault CMake infrastructure, see main README.md. 2 executables will be created, a serial one and an OpenMP multi-core one.

# 11. Instructions on how to run the code
Run the executable with the following command-line parameters
Usage: ./4_nbody_hermite4_serial

Available arguments: 
  -n=  --nbodies=    number of bodies [1024]
  -t=  --tend=       integration time [0.062500]
  -s=  --dstep=      print output every # steps [10]
  -e=  --eta=        accuracy parameter [0.400000]
  -Q=                virial ratio [1.000000]
  --fp32             toggle single precision [false]
  
# 12. Sample input(s)
Input-data is generated automatically when running the program.

# 13. Sample output(s)
