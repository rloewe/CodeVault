#include "petsc.h"
#include "petscfix.h"
/* memc.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petsc.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define petscmemcpy_ PETSCMEMCPY
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define petscmemcpy_ petscmemcpy
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define petscmemzero_ PETSCMEMZERO
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define petscmemzero_ petscmemzero
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   petscmemcpy_(void*a, void*b,size_t *n, int *__ierr ){
*__ierr = PetscMemcpy(a,b,*n);
}
void PETSC_STDCALL   petscmemzero_(void*a,size_t *n, int *__ierr ){
*__ierr = PetscMemzero(a,*n);
}
#if defined(__cplusplus)
}
#endif
