#include "petsc.h"
#include "petscfix.h"
/* factor.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscpc.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcfactorsetzeropivot_ PCFACTORSETZEROPIVOT
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcfactorsetzeropivot_ pcfactorsetzeropivot
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcfactorsetshiftnonzero_ PCFACTORSETSHIFTNONZERO
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcfactorsetshiftnonzero_ pcfactorsetshiftnonzero
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcfactorsetshiftpd_ PCFACTORSETSHIFTPD
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcfactorsetshiftpd_ pcfactorsetshiftpd
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   pcfactorsetzeropivot_(PC pc,PetscReal *zero, int *__ierr ){
*__ierr = PCFactorSetZeroPivot(
	(PC)PetscToPointer((pc) ),*zero);
}
void PETSC_STDCALL   pcfactorsetshiftnonzero_(PC pc,PetscReal *shift, int *__ierr ){
*__ierr = PCFactorSetShiftNonzero(
	(PC)PetscToPointer((pc) ),*shift);
}
void PETSC_STDCALL   pcfactorsetshiftpd_(PC pc,PetscTruth *shift, int *__ierr ){
*__ierr = PCFactorSetShiftPd(
	(PC)PetscToPointer((pc) ),*shift);
}
#if defined(__cplusplus)
}
#endif
