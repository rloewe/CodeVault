#include "petsc.h"
#include "petscfix.h"
/* stcg.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscksp.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define kspstcgsetradius_ KSPSTCGSETRADIUS
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define kspstcgsetradius_ kspstcgsetradius
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define kspstcggetquadratic_ KSPSTCGGETQUADRATIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define kspstcggetquadratic_ kspstcggetquadratic
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   kspstcgsetradius_(KSP ksp,PetscReal *radius, int *__ierr ){
*__ierr = KSPSTCGSetRadius(
	(KSP)PetscToPointer((ksp) ),*radius);
}
void PETSC_STDCALL   kspstcggetquadratic_(KSP ksp,PetscReal *quadratic, int *__ierr ){
*__ierr = KSPSTCGGetQuadratic(
	(KSP)PetscToPointer((ksp) ),quadratic);
}
#if defined(__cplusplus)
}
#endif
