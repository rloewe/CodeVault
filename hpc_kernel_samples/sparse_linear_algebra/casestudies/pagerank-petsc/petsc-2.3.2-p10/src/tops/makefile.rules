include ${PETSC_DIR}/bmake/common/base
include ${PETSC_DIR}/bmake/common/test

# Override the LANGUAGES variable settings below to reduce the 
# number of languages for which clients are generated; the 
# normal setting is all languages supported by Babel
#LANGUAGES = $(CCASPEC_BABEL_LANGUAGES)
LANGUAGES = cxx

CCASPEC_VARS = $(shell ${CCASPEC_CONFIG} --var CCASPEC_MAKEINCL)
include ${CCASPEC_VARS}

CFLAGS = ${BABEL_INCLUDE} 

SIDLFILES = ${PETSC_DIR}/src/tops/tops.sidl ${SIDL}

parse-check:
	${BABEL} -R${CCA_REPO} --parse-check ${SIDLFILES}

#-------------------------------------------------------------------------------
# Client targets

MYPREFIX = $(shell pwd)
SIDL_PACKAGES = $(basename $(notdir ${SIDLFILES} ) )
SIDL_PACKAGES_TARGETS = $(addprefix ., $(SIDL_PACKAGES))
LIBTOOL=$(CCASPEC_BABEL_BABEL_LIBTOOL)

XML_REP = -R$(CCASPEC_BABEL_XML_REPOSITORY)

clients: $(SIDL_PACKAGES_TARGETS)

$(SIDL_PACKAGES_TARGETS): 
	@for LANGUAGE in $(LANGUAGES); do \
		echo -e "\n### Building $$LANGUAGE client library for $(@:.%=%)."; \
		if [ ! -d client ] ; then mkdir client; fi; \
		if [ $$LANGUAGE == python ] ; then \
			$(BABEL) -c $$LANGUAGE $(XML_REP) $(SIDLFILES) -o client/python; \
		elif [ $$LANGUAGE == java ] ; then \
			$(BABEL) -c $$LANGUAGE $(XML_REP) $(SIDLFILES) -o client/java; \
		else \
			$(BABEL) -c $$LANGUAGE $(XML_REP) -l $(SIDLFILES) -o client; \
		fi; \
		$(SH) ./utils/generateClientMakefile.sh ${CCAFE_CONFIG} topsclient client ${PETSC_DIR} $$LANGUAGE;  \
		$(MAKE) -C client/$$LANGUAGE .lib ; \
	done;
	@touch $@

client-clean:
	-@$(RM) -r client  $(SIDL_PACKAGES_TARGETS)

client-distclean: client-clean
	-@$(RM) -r .repository .sidl .lib .\#* *~ lib include

client-debug:
	@echo $(SIDL_PACKAGES)
	@echo $(SIDL_PACKAGES_TARGETS)

#-------------------------------------------------------------------------------
# Server build

server-cxx:  server/cxx/obj/makefile 
	-@cd server/cxx/obj; make TLIBNAME=${TLIBNAME} libserver-cxx.${SOSUFFIX}

server/cxx/obj/makefile: ${SIDLFILES}
	-@if [ ! -d "server/cxx/obj" ]; then mkdir -p server/cxx/obj; fi
	-@if [ -d "server/cxx/SCCS" ]; then ${BK} edit -q server/cxx/*_Impl.cxx server/cxx/*_Impl.hxx; fi
	-${BABEL} -R ${CCA_REPO} --hide-glue \
		${SIDLEXCLUDE} --server=cxx --output-directory=./server/cxx ${SIDLFILES}
	@-${RM} ${REMOVEFILES} server/cxx/obj/makefile
	@-if [ ! -z "${TOPSCLIENT_LIB}" ]; then echo "TOPSCLIENT_LIB = ${TOPSCLIENT_LIB}" > server/cxx/obj/makefile; fi
	@-if [ ! -f "../babel.make" ]; then echo "include ../babel.make" >> server/cxx/obj/makefile; fi
	@-echo "include ${PETSC_DIR}/src/tops/makefile.rules" >> server/cxx/obj/makefile

libserver-cxx.${SOSUFFIX}: 
	@-${PCC} -c ${HAVE_CCA} ${PCC_FLAGS} ${COPTFLAGS} ${CFLAGS} ${CCPPFLAGS} -Iglue -I../glue -I.. \
	    -I${PETSC_DIR}/include/tops \
			$(CCASPEC_BABEL_CXXFLAGS) -I$(CCASPEC_BABEL_includedir) \
      -I$(CCASPEC_BABEL_includedir)/cxx -I$(CCASPEC_pkgincludedir)/cxx \
	    ../*.cxx ../glue/*.cxx
	@-${CC} -c ${HAVE_CCA} ${CC_FLAGS} ${COPTFLAGS} ${CFLAGS} ${CCPPFLAGS} \
			-I${PETSC_DIR}/include/tops -I../glue ../glue/*.c
	@-$(OMAKE) LIBNAME=${TLIBNAME} SHARED_LIBRARY_TMPDIR=. OTHERSHAREDLIBS="${PETSC_LIB} ${BABEL_LIB} ${SL_LINKER_LIBS} ${SYS_LIB} ${CC_LINKER_SLFLAG}${PETSC_LIB_DIR}/tops ${TOPSCLIENT_LIB}" shared_arch
	-@if [ ! -d "${PETSC_LIB_DIR}/cca" ]; then mkdir ${PETSC_LIB_DIR}/cca; fi
	-@for i in StructuredSolver UnstructuredSolver ; do \
	   ${PETSC_DIR}/src/tops/utils/genSCLCCA.sh cca ${PETSC_LIB_DIR}/libtops.${SL_LINKER_SUFFIX} \
		TOPS.$$i cxx dynamic global now TOPS.$$i > $$i.cca.in; \
	   sed -e /"@PETSC_LIB_DIR@/ s|@PETSC_LIB_DIR@|${PETSC_LIB_DIR}|" \
	       $$i.cca.in >  ${PETSC_LIB_DIR}/cca/$$i.cca; rm $$i.cca.in; \
	done;
#-------------------------------------------------------------------------------

cca: 
	-@echo "Generating *.cca files for ${EXNAME}"
	-@if [ "`grep SystemProxy ${EXNAME}.sidl | wc -l`" == "1" ]; then \
	     ${PETSC_DIR}/src/tops/utils/genSCLCCAmulti.sh cca ${PETSC_LIB_DIR}/lib${EXNAME}.${SL_LINKER_SUFFIX} \
		${EXNAME}.System cxx dynamic global now ${EXNAME}.System ${EXNAME}.SystemProxy > lib${EXNAME}.${SL_LINKER_SUFFIX}.cca.in; \
	else \
	     ${PETSC_DIR}/src/tops/utils/genSCLCCA.sh cca ${PETSC_LIB_DIR}/lib${EXNAME}.${SL_LINKER_SUFFIX} \
		${EXNAME}.System cxx dynamic global now ${EXNAME}.System > lib${EXNAME}.${SL_LINKER_SUFFIX}.cca.in; \
 	fi
	-@if [ ! -d "${PETSC_LIB_DIR}/cca" ]; then mkdir ${PETSC_LIB_DIR}/cca; fi
	-@sed -e /"@PETSC_LIB_DIR@/ s|@PETSC_LIB_DIR@|${PETSC_LIB_DIR}|" \
	    	-e /"@SOSUFFIX@/ s|@SOSUFFIX@|${SL_LINKER_SUFFIX}|" \
		lib${EXNAME}.${SL_LINKER_SUFFIX}.cca.in > ${PETSC_LIB_DIR}/cca/lib${EXNAME}.${SL_LINKER_SUFFIX}.cca
	@-${RM} *.cca.in
	-@echo "Creating simple Ccaffeine script"
	@-cd run; \
	if [ -f ${EXNAME}_rc.in ]; then\
	  sed -e /"@PETSC_LIB_DIR@/ s|@PETSC_LIB_DIR@|${PETSC_LIB_DIR}|" ${EXNAME}_rc.in > ${EXNAME}_rc ; fi; \
	if [ -f ${EXNAME}_proxy_rc.in ]; then\
	  sed -e /"@PETSC_LIB_DIR@/ s|@PETSC_LIB_DIR@|${PETSC_LIB_DIR}|" ${EXNAME}_proxy_rc.in > ${EXNAME}_proxy_rc ; fi ;\
	if [ -f ${EXNAME}_gui_rc.in ]; then\
	  sed -e /"@PETSC_LIB_DIR@/ s|@PETSC_LIB_DIR@|${PETSC_LIB_DIR}|" ${EXNAME}_gui_rc.in > ${EXNAME}_gui_rc; fi

test-cca: cca
	@-echo "Testing component example ${EXNAME} with Ccaffeine"
	@if [ -z "${CCA_PARALLEL}" ]; then \
	  cd run; \
	  ${CCAFE_HOME}/bin/ccafe-single --ccafe-rc ${EXNAME}_rc > ${EXNAME}_rc.log  2>&1 ; \
	  if [ "`egrep \"(specific|1 .+norm)\" ${EXNAME}_rc.log | wc -l`" -ge "2" ]; then \
	    echo "=== ${EXNAME}: Component test succeeded (see `pwd`/${EXNAME}_rc.log)."; \
	  else echo "*** ${EXNAME}: Component test failed (see `pwd`/${EXNAME}_rc.log)."; fi; \
        else \
	  cd run; \
          mpirun -np 2 ${CCAFE_HOME}/bin/ccafe-batch --ccafe-rc `pwd`/${EXNAME}_rc > `pwd`/${EXNAME}_rc.log  2>&1 ; \
	  if [ "`egrep \"(1 .+norm)\" ${EXNAME}_rc.log | wc -l`" -ge "1" ]; then \
	    echo "=== ${EXNAME}: Component test succeeded (see `pwd`/${EXNAME}_rc.log)."; \
	  else echo "*** ${EXNAME}: Component test failed (see `pwd`/${EXNAME}_rc.log)."; fi; \
	fi

gui-cca: cca
	@-echo "Running component example ${EXNAME} using Ccaffeine GUI (the output is in pOut0/pErr0)"
	@${CCAFE_HOME}/bin/gui-backend.sh --port 4242 --ccafe-rc ${EXNAME}_gui_rc > .ccafe.out 2>&1 & \
	echo -e "\nPlease wait...\n"; sleep 5; \
	${CCAFE_HOME}/bin/gui.sh --port 4242 # --scaleFont 1.5

kill-gui:
	@-echo "Cleaning up after GUI"
	@-${CCAFE_HOME}/bin/gui.sh --kill; ${CCAFE_HOME}/bin/gui-backend.sh --kill

#-------------------------------------------------------------------------------

examples: clients
	-@for dir in `ls -d examples/cxx/ex*`; do \
	  echo -e "\n### Building and testing in $$dir"; \
	  make -C $$dir server-cxx test-cca; \
	done

clean-cca: client-clean
	-@${RM} -r server/cxx/obj server/cxx/glue \
		examples/cxx/ex?/server/cxx/obj examples/cxx/ex?/server/cxx/glue 
#-------------------------------------------------------------------------------




.PHONY: examples
