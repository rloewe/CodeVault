
# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Valeriu Codreanu <valeriu.codreanu@surfsara.nl>
#
# ==================================================================================================
cmake_minimum_required(VERSION 2.8.10 FATAL_ERROR)
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/../../../cmake/Modules")
set(CMAKE_VERBOSE_MAKEFILE ON)

find_package(MKL)


include(${CMAKE_CURRENT_SOURCE_DIR}/../../../cmake/common.cmake)

# ==================================================================================================

if ("${DWARF_PREFIX}" STREQUAL "")
  set(DWARF_PREFIX 2_sparse)
endif()
set(NAME ${DWARF_PREFIX}_spgemm_mkl_shmem)

# ==================================================================================================
# C++ compiler settings

find_package(Common)

select_compiler_flags(cxx_flags
  GNU "-march=native"   # I suggest remove "-O3" as this is controlled by the CMAKE_BUILD_TYPE
  CLANG "-march=native" # same here
  Intel "-axavx2,avx")
set(CXX_FLAGS ${cxx_flags})
if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  set(CXX_FLAGS "${CXX_FLAGS} -Wall -Wno-comment")
  if(APPLE)
    set(CXX_FLAGS "${CXX_FLAGS} -Wa,-q")
  endif()
endif()
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CXX_FLAGS}")

# ==================================================================================================

# LUD with the MKL library
if (MKL_FOUND)
	include_directories(${MKL_INCLUDE_DIR})
	link_directories(${MKL_LIBRARY_DIR})
	add_executable(${NAME} mklspgemm.c)
	target_link_libraries(${NAME} mkl_intel_lp64 mkl_sequential mkl_core pthread m)
	install(TARGETS ${NAME} DESTINATION bin)
else ()
	message("## Skipping '${NAME}': no MKL support found")
  install(CODE "MESSAGE(\"${NAME} can only be built with MKL.\")")
endif()

unset(NAME)

# ==================================================================================================
