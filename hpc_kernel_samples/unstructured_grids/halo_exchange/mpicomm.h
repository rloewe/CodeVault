#ifndef MPICOMM_H
#define MPICOMM_H

#include <mpi.h>
#include "configuration.h"
#include "box.h"
#include "mesh.h"
#include "field.h"

void mpi_broadcast_configuration_start(conf_t *configuration, MPI_Request *request);
void mpi_broadcast_configuration_finish(MPI_Request *request);

void mpi_broadcast_decomposition(box_t *decomposition);

void mpi_create_graph_communicator(const mesh_t *mesh, const conf_t *configuration, MPI_Comm *graph_comm);

void mpi_halo_exchange_int_sparse_collective(int_field_t *field);
void mpi_halo_exchange_int_collective(int_field_t *field);
void mpi_halo_exchange_int_p2p_default(int_field_t *field);
void mpi_halo_exchange_int_p2p_synchronous(int_field_t *field);
void mpi_halo_exchange_int_p2p_ready(int_field_t *field);

#endif
