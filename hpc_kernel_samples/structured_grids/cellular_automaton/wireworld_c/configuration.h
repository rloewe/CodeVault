#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <stdio.h>

enum verbosity_level_enum
{
   OFF = 0,
   INFO = 1,
   DEBUG = 2,
   TRACE = 3
};

enum transmission_mode_enum
{
   SPARSE_COLLECTIVE = 0,
   POINT_TO_POINT = 1,
   PERSISTENT_REQUEST = 2
};

enum communication_computation_mode_enum
{
   NO_OVERLAP = 0,
   OVERLAP = 4
};

#define FILE_BASENAME_SZ 1024
#define FILE_EXT ".wi"
#define FILE_EXT_LEN (sizeof(FILE_EXT)-1)
#define FILE_NAME_SZ (FILE_BASENAME_SZ + FILE_EXT_LEN)

typedef struct
{
   int verbosity_level;
   int transmission_mode;
   int communication_computation_mode;

   int nprocs[2];

   int n_iterations;
   long n_generations_per_iteration;

   char file_basename[FILE_BASENAME_SZ];
} conf_t;

// Meta-information for MPI-Datatype creation (see mpitypes.c)
#define CONF_T_N_INT_MEMBERS 6
#define CONF_T_FIRST_INT_MEMBER verbosity_level

#define CONF_T_N_LONG_MEMBERS 1
#define CONF_T_FIRST_LONG_MEMBER n_generations_per_iteration

#define CONF_T_N_CHAR_MEMBERS FILE_BASENAME_SZ
#define CONF_T_FIRST_CHAR_MEMBER file_basename[0]

void conf_init_default(conf_t *c);

void conf_init_from_args(conf_t *c, int argc, char* argv[]);

void conf_print(const conf_t *c, FILE *file);

int info_enabled(const conf_t *c);
int warn_enabled(const conf_t *c);
int debug_enabled(const conf_t *c);
int trace_enabled(const conf_t *c);

#endif

